import Script from "next/script";

import 'bootstrap/dist/css/bootstrap.css'
import '../styles/globals.scss'

function MyApp({ Component, pageProps }) {
  return (
      <>
        <Script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js" strategy="lazyOnload"/>

        <Component {...pageProps} />
      </>
  )
}

export default MyApp
