import Image from 'next/image';

import styles from './Contact.module.scss'
import DownloadIcon from "../../public/images/contact.png";

export default function Contact() {

    return (
        <div className={`container ${styles.container}`}>
            <h2 className={'text-center h1'}>
                Get In Touch
            </h2>

            <p className={`text-center text-secondary m-auto ${styles.desc}`}>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore.
            </p>

            <section className='d-flex flex-column flex-lg-row flex-xl-row flex-xl-row mt-4'>

                <div className={styles.itemDownload}>
                    <form>
                        <div className="row">
                            <div className={`col-lg-6 col-md-6 col-xl-6 ${styles.inputContainer}`}>
                                <input placeholder={'Name'} type={'text'} required
                                       className={`form-control form-control-lg ${styles.input}`}/>
                            </div>
                            <div className={`col-lg-6 col-md-6 col-xl-6 ${styles.inputContainer}`}>
                                <input placeholder={'Subject'} type={'text'} required
                                       className={`form-control form-control-lg ${styles.input}`}/>
                            </div>
                            <div className={`col-lg-6 col-md-6 col-xl-6 ${styles.inputContainer}`}>
                                <input placeholder={'Email'} type={'text'} required
                                       className={`form-control form-control-lg ${styles.input}`}/>
                            </div>
                            <div className={`col-lg-6 col-md-6 col-xl-6 ${styles.inputContainer}`}>
                                <input placeholder={'Budget'} type={'text'} required
                                       className={`form-control form-control-lg ${styles.input}`}/>
                            </div>
                            <div className={`col-lg-12 col-md-12 col-xl-12 ${styles.inputContainer}`}>
                                <textarea placeholder={'Write Message'} cols={6} required
                                       className={`form-control form-control-lg ${styles.input}`}/>
                            </div>
                        </div>

                        <div>
                            <button type="submit" className={'btn btn-success btn-shadow-up btn-lg mt-2'}>
                                SUBMIT
                            </button>
                        </div>
                    </form>
                </div>

                <div className={styles.itemDownloadImage}>
                    <Image src={DownloadIcon}/>
                </div>
            </section>

        </div>
    )
}
