import {faCog, faPaintBrush, faHeart} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

import styles from './Features.module.css'

export default function Features() {
    const data = [
        {
            title: 'Bootstrap 4',
            desc: 'Share processes and data secure lona need to know basis Our team assured your web site is always safe.',
            icon: faCog,
        },
        {
            title: 'Slick Design',
            desc: 'Share processes and data secure lona need to know basis Our team assured your web site is always safe.',
            icon: faPaintBrush,
        },
        {
            title: 'Crafted with Love',
            desc: 'Share processes and data secure lona need to know basis Our team assured your web site is always safe.',
            icon: faHeart,
        },
    ]

    return (
        <section className={`container d-flex flex-row flex-wrap ${styles.containerFeature}`}>
            {data.map(item => {
                return (
                    <FeatureCardItem
                        key={item}
                        item={item}
                    />
                )
            })}
        </section>
    )
}

function FeatureCardItem({item}) {
    const {title, desc, icon} = item;

    return (
        <div className={`card ${styles.cardFeature}`}>
            <div className={styles.cardImageContainerFeature}>
                <FontAwesomeIcon icon={icon} color={'#50cf8e'}/>
            </div>

            <div className={'card-body'}>
                <h5 className={'card-title d-flex justify-content-center fw-bold'}>
                    {title}
                </h5>

                <p className={'card-text text-center text-secondary'}>
                    {desc}
                </p>
            </div>
        </div>
    )
}
