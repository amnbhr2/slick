import Image from 'next/image';

import Intro from '../../public/images/intro.png'
import styles from './Header.module.css';

export default function Header() {
    return (
        <header className='d-flex flex-column flex-lg-row flex-xl-row flex-xl-row bg-primary'>
            <div className='p-1 mt-200'>
                <h1 className='text-white pt-4 lh-base'>
                    Handcrafted Web Template
                    For Business and Startups
                </h1>

                <p className={`text-white lh-base ${styles.headerDescription}`}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab
                    dolores ea fugiat nesciunt quisquam.
                </p>

                <div className={'d-flex flex-column flex-sm-column flex-md-row flex-lg-row flex-xl-row flex-xl-row'}>
                    <button className={'btn btn-success-outline-hover btn-lg'}>
                        GET STARTED
                    </button>

                    <button className={`btn btn-shadow-up btn-light btn-lg ${styles.headerButton} text-success`}>
                        CONTACT US
                    </button>
                </div>
            </div>
            <div className='p-1 mt-200'>
                <Image src={Intro}/>
            </div>
        </header>
    )
}
