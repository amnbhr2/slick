import {
    faCoffee,
    faBriefcase,
    faLayerGroup,
    faHandSparkles,
    faTrashRestore,
    faAudioDescription
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

import styles from './Services.module.css'
import {useState} from "react";

export default function Services() {
    const data = [
        {
            title: 'Easy to Customize',
            desc: 'Producing long lasting organic SEO results for brand of different kinds for more than a decade, we understand that’s your.',
            icon: faCoffee,
        },
        {
            title: 'Business Template',
            desc: 'Producing long lasting organic SEO results for brand of different kinds for more than a decade, we understand that’s your.',
            icon: faBriefcase,
        },
        {
            title: 'Clean & Trendy Design',
            desc: 'Producing long lasting organic SEO results for brand of different kinds for more than a decade, we understand that’s your.',
            icon: faHandSparkles,
        },
        {
            title: 'Tons of Sections',
            desc: 'Producing long lasting organic SEO results for brand of different kinds for more than a decade, we understand that’s your.',
            icon: faLayerGroup,
        },
        {
            title: 'Free Future Updates',
            desc: 'Producing long lasting organic SEO results for brand of different kinds for more than a decade, we understand that’s your.',
            icon: faTrashRestore,
        },
        {
            title: 'Premier Support',
            desc: 'Producing long lasting organic SEO results for brand of different kinds for more than a decade, we understand that’s your.',
            icon: faAudioDescription,
        },
    ]

    return (
        <div className={styles.container}>
            <h2 className={'text-center h1'}>
                Services We Provide
            </h2>

            <p className={`text-center text-secondary m-auto ${styles.desc}`}>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore.
            </p>

            <section className={`container d-flex flex-md-row flex-lg-row flex-xl-row flex-xxl-row flex-sm-column flex-wrap ${styles.itemsContainer}`}>
                {data.map(item => {
                    return (
                        <ServiceCardItem
                            key={item}
                            item={item}
                        />
                    )
                })}
            </section>
        </div>
    )
}

function ServiceCardItem({item}) {
    const {title, desc, icon} = item;

    const [itemHover, setItemHover] = useState(false)

    return (
        <div className={`d-flex flex-xxl-row flex-xl-row flex-lg-row flex-md-row flex-sm-column flex-column align-items-center ${styles.card}`}
             onMouseEnter={() => setItemHover(true)}
             onMouseLeave={() => setItemHover(false)}>

            <div
                className={`d-flex ${itemHover ? styles.iconContainerHover : styles.iconContainer}`}>
                <FontAwesomeIcon icon={icon}/>
            </div>

            <div className={styles.itemDetails}>
                <h5 className={'mb-4'}>
                    {title}
                </h5>

                <p className={'text-secondary'}>
                    {desc}
                </p>
            </div>
        </div>
    )
}
