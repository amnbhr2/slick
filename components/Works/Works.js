import {useRef, useState} from "react";

import Image from 'next/image';

import styles from './Works.module.css'
import image1 from '../../public/images/01.jpg'
import image2 from '../../public/images/02.jpg'
import image3 from '../../public/images/03.jpg'
import image4 from '../../public/images/04.jpg'
import image5 from '../../public/images/05.jpg'

export default function Works() {
    const data = [
        {
            image: image1,
        },
        {
            image: image2,
        },
        {
            image: image3,
        },
        {
            image: image4,
        },
        {
            image: image5,
        },
        {
            image: image1,
        },
        {
            image: image2,
        },
        {
            image: image3,
        },
        {
            image: image4,
        },
        {
            image: image5,
        },
    ]

    return (
        <div>
            <div className={styles.container}>
                <h2 className={'text-center h1 text-white'}>
                    Recent Works
                </h2>

                <p className={`text-center text-white m-auto ${styles.desc}`}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore.
                </p>

            </div>

            <section className={`container ${styles.itemsContainer}`}>

                <div className={styles.sliderInner}>
                    {data.map(item => {
                        return (
                            <div key={item.image.toString()}
                                 className={`${styles.card}`}>

                                <Image  src={item.image}/>
                            </div>
                        )
                    })}
                </div>
            </section>
        </div>
    )
}
