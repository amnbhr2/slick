import Image from 'next/image';

import styles from './Video.module.css'
import image from '../../public/images/man_landscape.jpg'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faPlay,
} from '@fortawesome/free-solid-svg-icons'
import {useState} from "react";

export default function Video() {

    const [itemHover, setItemHover] = useState(false)

    return (
        <div className={'d-flex flex-column align-items-center'}>
            <div className={styles.container}>
               <Image src={image} height={'650px'}/>

                <div className={styles.iconContainer}
                     onMouseEnter={() => setItemHover(true)}
                     onMouseLeave={() => setItemHover(false)}>

                    <FontAwesomeIcon className={styles.icon} icon={faPlay} color={itemHover ? '#ffffff' : '#50cf8e'}/>
                </div>
            </div>

            <section className={`${styles.content}`}>

            </section>
        </div>
    )
}
