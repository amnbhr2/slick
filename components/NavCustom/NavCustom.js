import {useEffect, useState} from "react";

import Link from "next/link";
import Image from "next/image";

import Logo from '../../public/images/logo.png'
import styles from './NavCustom.module.scss'

export default function NavCustom() {

    const [scrolled, setScrolled] = useState(false)

    useEffect(() => {
        window.addEventListener('scroll', onScroll, false)
    }, [])

    return (
        <nav id="navBar" className={`navbar fixed-top navbar-expand-lg navbar-dark bg-primary ${scrolled ? 'nav-shadow' : ''}`}>
            <div className='container'>
                <Link className="navbar-brand" href="/">
                    <Image src={Logo} height="68" width="165" alt="Slick"/>
                </Link>

                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
                        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>

                <div className="collapse navbar-collapse" id="navbarText">
                    <div className={styles.navItemsContainer}>
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <a className="nav-link">Home</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link">About</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link">Services</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link">Showcase</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link">Pricing</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link">Team</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link">Blog</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link">Contact</a>
                            </li>
                        </ul>

                        <button type="button" className="btn btn-shadow btn-nav-bar">
                            Download
                        </button>
                    </div>
                </div>
            </div>
        </nav>
    )

    function onScroll() {
        if (window.scrollY > 10) {
            setScrolled(true)
        } else {
            setScrolled(false)
        }
    }
}
