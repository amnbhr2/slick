import Image from 'next/image';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faPersonBooth, faCalendar} from "@fortawesome/free-solid-svg-icons";

import styles from './Blog.module.scss'
import image_1 from '../../public/images/01_b.jpg'
import image_2 from '../../public/images/02_b.jpg'
import image_3 from '../../public/images/03_b.jpg'
import {useState} from "react";

export default function Blog() {
    const data = [
        {
            name: 'How Slick Will Transform Your Business',
            desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.',
            image: image_1,
        },
        {
            name: 'Growth Techniques for New Startups',
            desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.',
            image: image_2,
        },
        {
            name: 'Writing Professional Emails to Customers',
            desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.',
            image: image_3,
        },
    ]

    return (
        <div className={styles.container}>
            <h2 className={'text-center h1'}>
                Latest Blog Posts
            </h2>

            <p className={`text-center text-secondary m-auto ${styles.desc}`}>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore.
            </p>


            <section className={`container d-flex flex-row flex-wrap ${styles.containerFeature}`}>
                {data.map((item, index) => {
                    return (
                        <BlogCardItem
                            key={index}
                            item={item}
                        />
                    )
                })}
            </section>

        </div>
    )
}

function BlogCardItem({item}) {
    const {name, desc, image} = item;

    const [itemHover, setItemHover] = useState(false)

    return (
        <div className={`card ${styles.cardFeature}`}
             onMouseEnter={() => setItemHover(true)}
             onMouseLeave={() => setItemHover(false)}>
            <Image src={image}/>

            <div className={`card-body ${styles.body}`}>

                <h5 className={'card-title d-flex justify-content-center fw-bold blogCardTitle'}>
                    {name}
                </h5>

                <p className={`ard-text text-secondary`}>
                    {desc}
                </p>

                <h6 className={'text-success'}>
                    5 MIN READ
                </h6>

            </div>

            <hr/>

            <div className={'d-flex card-footer w-100 bg-white p-4 justify-content-between'}>
                <div className={`d-flex flex-row align-items-center ${styles.iconRoot}`}>
                    <div className={styles.iconContainer}>
                        <FontAwesomeIcon icon={faPersonBooth} color={'#50cf8e'}/>
                    </div>
                    <p className={`text-secondary ${styles.cardFooterItem}`}>
                        Posted by Admin
                    </p>
                </div>
                <div className={`d-flex flex-row align-items-center ${styles.iconRoot}`}>
                    <div className={styles.iconContainer}>
                        <FontAwesomeIcon icon={faCalendar} color={'#50cf8e'}/>
                    </div>
                    <p className={`text-secondary ${styles.cardFooterItem}`}>
                        10 April, 2020
                    </p>
                </div>
            </div>
        </div>
    )
}
