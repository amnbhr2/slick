import {faAngleUp} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

import styles from './Up.module.css'
import {useEffect, useState} from "react";

export default function Up({onPress}) {

    const [scrolled, setScrolled] = useState(false)

    useEffect(() => {
        window.addEventListener('scroll', onScroll, false)
    }, [])

    if (!scrolled) {
        return null
    }

    return (
        <div onClick={() => window.scrollTo(0, 0)} className={`d-flex justify-content-center ${styles.containerUp}`}>
            <FontAwesomeIcon icon={faAngleUp} color={'#fff'}/>
        </div>
    )

    function onScroll() {
        if (window.scrollY > 10) {
            setScrolled(true)
        } else {
            setScrolled(false)
        }
    }
}
