import Image from 'next/image';

import DownloadIcon from '../../public/images/download.png'
import styles from './Download.module.css';

export default function Download() {
    return (
        <section className='d-flex flex-column flex-lg-row flex-xl-row flex-xl-row'>
            <div className={styles.itemDownload}>
                <Image src={DownloadIcon}/>
            </div>

            <div className={styles.itemDownload}>
                <h1 className='pt-4 lh-base'>
                    Crafted For Business, Startup and Agency Websites
                </h1>

                <p className={`text-secondary lh-base ${styles.headerDescription}`}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                    veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                    commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                    velit esse cillum dolore eu fugiat nulla pariatur.
                </p>

                <div>
                    <button className={'btn btn-success btn-shadow-up btn-lg'}>
                        DOWNLOAD
                    </button>
                </div>
            </div>

        </section>
    )
}
