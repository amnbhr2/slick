import Image from 'next/image';

import icon from '../../public/images/footer-logo.png'
import styles from './Footer.module.css';

export default function Footer() {
    return (
        <footer className={styles.container}>
            <div className='container d-flex flex-column flex-sm-column flex-md-column flex-lg-row flex-xl-row align-items-start'>
                <div className={styles.itemOne}>
                    <div style={{marginTop: '50px'}}>
                        <Image src={icon}/>
                    </div>
                </div>
                <div className={styles.itemTwo}>
                    <h5 className={'text-white mb-4'}>
                        Company
                    </h5>

                    <ul className={styles.list}>
                        <li className={styles.listItem}>
                            - About Us
                        </li>
                        <li className={styles.listItem}>
                            - Career
                        </li>
                        <li className={styles.listItem}>
                            - Blog
                        </li>
                        <li className={styles.listItem}>
                            - Press
                        </li>
                    </ul>
                </div>
                <div className={styles.itemTwo}>
                    <h5 className={'text-white mb-4'}>
                        Product
                    </h5>
                    <ul className={styles.list}>
                        <li className={styles.listItem}>
                            - Customer Service
                        </li>
                        <li className={styles.listItem}>
                            - Enterprise
                        </li>
                        <li className={styles.listItem}>
                            - Price
                        </li>
                        <li className={styles.listItem}>
                            - Security
                        </li>
                        <li className={styles.listItem}>
                            - Why SLICK?
                        </li>
                    </ul>
                </div>
                <div className={styles.itemTwo}>
                    <h5 className={'text-white mb-4'}>
                        Download App
                    </h5>
                    <ul className={styles.list}>
                        <li className={styles.listItem}>
                            - Android App
                        </li>
                        <li className={styles.listItem}>
                            - IOS App
                        </li>
                        <li className={styles.listItem}>
                            - Windows App
                        </li>
                        <li className={styles.listItem}>
                            - Play Store
                        </li>
                        <li className={styles.listItem}>
                            - IOS Store
                        </li>
                    </ul>
                </div>
                <div className={styles.itemOne}>
                    <h5 className={'text-white mb-4'}>
                        Subscribe Now
                    </h5>
                    <p className={styles.p}>
                        Appropriately implement calysts for change visa wireless catalysts for change.
                    </p>

                    <div className="input-group mb-3 mt-3">
                        <input type="text" className={`form-control ${styles.input}`} placeholder="Enter Email"
                               aria-describedby="button-addon2"/>
                        <button className="btn btn-success" type="button">
                            Send
                        </button>
                    </div>

                </div>
            </div>

            <hr style={{backgroundColor: '#371d65'}}/>

            <div className={'p-4 d-flex justify-content-center align-items-center'}>
                <h5 className={'text-success text-center'}>
                    Crafted by <span className={'text-white'}>AAJGroup</span>
                </h5>
            </div>
        </footer>
    )
}
