import styles from './Pricing.module.css'

export default function Pricing() {
    const data = [
        {
            title: 'FREE',
            subTitle: '00',
            items: ['Free Instalation', '500MB Storage', 'Single User', '5 GB Bandwith', 'Minimal Features', 'No Dashboard']
        },
        {
            title: 'STANDARD',
            subTitle: '19.99',
            items: ['Free Instalation', '2 GB Storage', 'Upto 2 users', '50 GB Bandwith', 'All Features', 'Sales Dashboard']
        },
        {
            title: 'BUSINESS',
            subTitle: '29.99',
            items: ['Free Instalation', '5 GB Storage', 'Upto 4 users', '100 GB Bandwith', 'All Features', 'Sales Dashboard']
        },
    ]

    return (
        <div>
            <h2 className={'text-center h1'}>
                Pricing Plans
            </h2>

            <p className={`text-center text-secondary m-auto ${styles.desc}`}>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore.
            </p>


            <section className={`container d-flex flex-row flex-wrap ${styles.containerFeature}`}>
                {data.map((item, index) => {
                    return (
                        <PricingCardItem
                            key={item}
                            index={index}
                            item={item}
                        />
                    )
                })}
            </section>

        </div>
    )
}

function PricingCardItem({item, index}) {
    const {title, subTitle, items} = item;

    return (
        <div className={`card ${styles.cardFeature}`}>
            <div className={'card-body'}>
                <h1 className='text-center'/>

                <h5 className={'card-title d-flex justify-content-center fw-bold'}>
                    {title}
                </h5>

                <div className={'d-flex flex-row justify-content-center align-items-center'}>
                    <span className={styles.pricePrefix}>$</span>

                    <p className={`text-center ${styles.price}`}>
                        {subTitle}
                    </p>

                </div>

                <ul className={styles.list}>
                    {items.map(item => {
                        return(
                            <li key={item} className={`text-secondary mb-2 text-center w-100`}>
                                {item}
                            </li>
                        )
                    })}
                </ul>

            </div>

            <button className={`btn ${index === 1 ? 'btn-shadow-up btn-lg btn-success' : 'btn-outline-success'}`}>
                PURCHASE
            </button>
        </div>
    )
}
