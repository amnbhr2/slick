import Image from 'next/image';

import {faFacebook, faTwitter, faGoogle} from '@fortawesome/free-brands-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

import styles from './Team.module.css'
import image_1 from '../../public/images/01_p.jpg'
import image_2 from '../../public/images/02_p.jpg'
import image_3 from '../../public/images/03_p.jpg'
import image_4 from '../../public/images/04_p.jpg'
import {useState} from "react";

export default function Team() {
    const data = [
        {
            name: 'Patric Green',
            career: 'Lead Designer',
            image: image_1,
        },
        {
            name: 'Celina D Cruze',
            career: 'Front-end Developer',
            image: image_2,
        },
        {
            name: 'Daryl Dixon',
            career: 'Content Writer',
            image: image_3,
        },
        {
            name: 'Mark Parker',
            career: 'Support Engineer',
            image: image_4,
        },
    ]

    return (
        <div>
            <h2 className={'text-center h1 mt-200'}>
                Team Members
            </h2>

            <p className={`text-center text-secondary m-auto ${styles.desc}`}>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore.
            </p>


            <section className={`container d-flex flex-row flex-wrap ${styles.containerFeature}`}>
                {data.map((item, index) => {
                    return (
                        <TeamCardItem
                            key={index}
                            item={item}
                        />
                    )
                })}
            </section>

        </div>
    )
}

function TeamCardItem({item}) {
    const {name, career, image} = item;

    const [itemHover, setItemHover] = useState(0)

    return (
        <div className={`card ${styles.cardFeature}`}>
            <Image className={styles.image} src={image}/>

            <div className={`d-flex flex-row ${styles.iconsRoot}`}>
                <div className={styles.iconContainer}
                     onMouseEnter={() => setItemHover(1)}
                     onMouseLeave={() => setItemHover(0)}>
                    <FontAwesomeIcon className={styles.icon} icon={faFacebook} color={itemHover === 1 ? '#ffffff' : '#50cf8e'}/>
                </div>
                <div className={styles.iconContainer}
                     onMouseEnter={() => setItemHover(2)}
                     onMouseLeave={() => setItemHover(0)}>
                    <FontAwesomeIcon className={styles.icon} icon={faTwitter} color={itemHover === 2 ? '#ffffff' : '#50cf8e'}/>
                </div>
                <div className={styles.iconContainer}
                     onMouseEnter={() => setItemHover(3)}
                     onMouseLeave={() => setItemHover(0)}>
                    <FontAwesomeIcon className={styles.icon} icon={faGoogle} color={itemHover === 3 ? '#ffffff' : '#50cf8e'}/>
                </div>
            </div>

            <div className={'card-body'}>
                <h1 className='text-center'/>

                <h6 className={'card-title d-flex justify-content-center fw-bold'}>
                    {name}
                </h6>

                <p className={`text-center card-text text-secondary ${styles.career}`}>
                    {career}
                </p>
            </div>
        </div>
    )
}
